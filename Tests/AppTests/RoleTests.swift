//
//  RoleTests.swift
//  App
//
//  Created by Eric Jacobsen on 8/26/18.
//

@testable import App
import Vapor
import XCTest
import FluentPostgreSQL
import Authentication


final class RoleTests: XCTestCase {
    var app: Application!
    var conn: PostgreSQLConnection!
    
    override func setUp() {
        try! Application.reset()
        app = try! Application.testable()
        conn = try! app.newConnection(to: .psql).wait()
    }
    
    override func tearDown() {
        conn.close()
    }
    
    private func createUserWithRole(_ role: Role) throws -> Future<User> {
        let hashedPassword = try BCrypt.hash("ICU81MI")
        return User(id: nil, name: "Herp Derp", password: hashedPassword, role: role).save(on: conn)
    }
    
    private func requestPage(_ url: String, as user: User?) throws -> HTTPStatus {
        var headers = HTTPHeaders()
        headers.replaceOrAdd(name: .contentType, value: "application/json")
        return try app.getResponseStatus(to: url, method: .GET, headers: headers, loggedInUser: user)
   }

    func testNoUser() throws {
        let tryBasicPage = try requestPage("/v1/basic", as: nil)
        XCTAssertEqual(tryBasicPage, HTTPStatus.unauthorized, "Page with any role restriction requires User:RoleAuthorizable.")
        
        let tryEditorPage = try requestPage("/v1/editor", as: nil)
        XCTAssertEqual(tryEditorPage, HTTPStatus.unauthorized, "Page with any role restriction requires User:RoleAuthorizable.")
        
        let tryAdminPage = try requestPage("/v1/admin", as: nil)
        XCTAssertEqual(tryAdminPage, HTTPStatus.unauthorized, "Page with any role restriction requires User:RoleAuthorizable.")
    }
    
    
    func testBasicUser() throws {
        let user = try self.createUserWithRole(.basic).wait()
        
        let tryBasicPage = try requestPage("/v1/basic", as: user)
        XCTAssertEqual(tryBasicPage, HTTPStatus.ok, "User.role = basic should be able to access a page with .basic role restriction.")
        
        let tryEditorPage = try requestPage("/v1/editor", as: user)
        XCTAssertEqual(tryEditorPage, HTTPStatus.forbidden, "User.role = basic should NOT be able to access a page with .editor role restriction.")
        
        let tryAdminPage = try requestPage("/v1/admin", as: user)
        XCTAssertEqual(tryAdminPage, HTTPStatus.forbidden, "User.role = basic should NOT be able to access a page with .admin role restriction.")
    }
    
    
    func testEditorUser() throws {
        let editor = try self.createUserWithRole(.editor).wait()

        let tryBasicPage = try requestPage("/v1/basic", as: editor)
        XCTAssertEqual(tryBasicPage, HTTPStatus.ok, "User.role = editor should be able to access a page with .basic role restriction.")
        
        let tryEditorPage = try requestPage("/v1/editor", as: editor)
        XCTAssertEqual(tryEditorPage, HTTPStatus.ok, "User.role = editor should be able to access a page with .editor role restriction.")
        
        let tryAdminPage = try requestPage("/v1/admin", as: editor)
        XCTAssertEqual(tryAdminPage, HTTPStatus.forbidden, "User.role = editor should NOT be able to access a page with .admin role restriction.")
    }
    
    
    func testAdminUser() throws {
        let admin = try self.createUserWithRole(.admin).wait()

        let tryBasicPage = try requestPage("/v1/basic", as: admin)
        XCTAssertEqual(tryBasicPage, HTTPStatus.ok, "User.role = admin should be able to access a page with .basic role restriction.")

        let tryEditorPage = try requestPage("/v1/editor", as: admin)
        XCTAssertEqual(tryEditorPage, HTTPStatus.ok, "User.role = admin should be able to access a page with .editor role restriction.")
        
        let tryAdminPage = try requestPage("/v1/admin", as: admin)
        XCTAssertEqual(tryAdminPage, HTTPStatus.ok, "User.role = admin should be able to access a page with .admin role restriction.")

    }
    

    
}
