//
//  Role.middleware.swift
//  App
//
//  Created by Eric Jacobsen on 8/7/18.
//

import Vapor
import Authentication

// TODO: RoleMiddleware.init should maybe carry user as self.user

class RoleMiddleware<A: Authenticatable & RoleAuthorizable, R>: Middleware where A.RoleType == R {
    var accessibleRoles: [R]?
    
    required init(_ roles: [R]? = nil) {
        self.accessibleRoles = roles
    }
    
    func respond(to req: Request, chainingTo next: Responder) throws -> Future<Response> {
        if let accessibleRoles = self.accessibleRoles {
            guard let user = try? req.requireAuthenticated(A.self) else {
                throw Abort(.unauthorized, reason: "A user must be logged in.")
            }
            if !accessibleRoles.contains(user[keyPath: A.roleKey]) {
                throw Abort(.forbidden, reason: "This user does not have the necessary role.")
            }
        }
        return try next.respond(to: req)
    }
}


extension RoleMiddleware: ServiceType {
    static func makeService(for worker: Container) throws -> Self {
        return .init()
    }
}


protocol RoleAuthorizable {
    associatedtype RoleType: Hashable
    typealias RoleKey = WritableKeyPath<Self, RoleType>
    static var roleKey: RoleKey { get }
}

