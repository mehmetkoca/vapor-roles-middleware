//
//  User.swift
//  App
//
//  Created by Eric Jacobsen on 8/26/18.
//

import Vapor
import FluentPostgreSQL
import Authentication


final class User: Codable, Content, PostgreSQLUUIDModel, PostgreSQLMigration {
    var id: UUID?
    var name: String
    var password: String
    var role: Role = .basic
    
    init(
        id: UUID?,
        name: String,
        password: String,
        role: Role = .basic
        ) {
        self.id = id
        self.name = name
        self.password = password
        self.role = role
    }
}


// MARK: Role Authorization

extension User: RoleAuthorizable {
    typealias RoleType = Role
    static var roleKey: RoleKey = \.role
}


// MARK: Basic Authentication

extension User: BasicAuthenticatable {
    static let usernameKey: UsernameKey = \User.name
    static let passwordKey: PasswordKey = \User.password
}


