import Vapor
import Authentication
import FluentPostgreSQL

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    /// Register providers first
    try services.register(FluentPostgreSQLProvider())

    try services.register(AuthenticationProvider())

    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    /// Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    /// middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    /// Register the configured SQLite database to the database config.
    var databases = DatabasesConfig()
    databases.add(database: PostgreSQLDatabase(config: PostgreSQLDatabaseConfig(
        hostname: Environment.get("DATABASE_HOST") ?? "localhost",
        port: (env == .testing) ? 5433 : 5432,
        username: Environment.get("DATABASE_USER") ?? "vapor",
        database: Environment.get("DATABASE_DB") ?? "vapor",
        password: Environment.get("DATABASE_PASSWORD") ?? "password"
    )), as: .psql)
    if env == .testing {
        databases.enableLogging(on: .psql)
    }
    services.register(databases)
    
    /// Configure migrations
    var migrations = MigrationConfig()
    migrations.add(migration: Role.self, database: .psql)
    migrations.add(model: User.self, database: .psql)
    services.register(migrations)

    //register fluent commands
    var commandConfig = CommandConfig.default()
    commandConfig.useFluentCommands()
    services.register(commandConfig)
    
}
